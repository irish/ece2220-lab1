/* Michael Smith <mjs3@clemson.edu>
 * Lab 1 - Tic Tac Toe Game.
 */

#include <stdio.h>
#include <stdlib.h>
#include "tictactoe.h"

void playGame(char *boardArray) {
	int turn;
	int player;
	char input;

	boardArray[0] = '1';
	boardArray[1] = '2';
	boardArray[2] = '3';
	boardArray[3] = '4';
	boardArray[4] = '5';
	boardArray[5] = '6';
	boardArray[6] = '7';
	boardArray[7] = '8';
	boardArray[8] = '9';

	for (turn = 0; turn < 9; turn++) {
		player = (turn % 2) + 1;
		playerTurn(boardArray, player);
	}

	printf("No Winner! Do you want to play again? r/R to restart. Anything else quits.: ");
	scanf("%c", &input);

	if (input == 'r' || input == 'R') {
		playGame(boardArray);
	} else {
		exit(0);
	}
}

/* Prints out the board along with tokens in each spot. */
void printBoard(char *boardArray) {
	printf("-------\n");
	printf("|%c|%c|%c|\n", boardArray[0], boardArray[1], boardArray[2]);
	printf("-------\n");
	printf("|%c|%c|%c|\n", boardArray[3], boardArray[4], boardArray[5]);
	printf("-------\n");
	printf("|%c|%c|%c|\n", boardArray[6], boardArray[7], boardArray[8]);
	printf("-------\n");
}

/* Check board state to determine if a player has one. */
void winCheck(char *boardArray, int player) {
	char input = '_';

	if (((boardArray[0] == boardArray[1]) && (boardArray[0] == boardArray[2]))
			|| ((boardArray[3] == boardArray[4]) && (boardArray[3] == boardArray[5]))
			|| ((boardArray[6] == boardArray[7]) && (boardArray[6] == boardArray[8]))
			|| ((boardArray[0] == boardArray[3]) && (boardArray[0] == boardArray[6]))
			|| ((boardArray[1] == boardArray[4]) && (boardArray[1] == boardArray[7]))
			|| ((boardArray[2] == boardArray[5]) && (boardArray[2] == boardArray[8]))
			|| ((boardArray[0] == boardArray[4]) && (boardArray[0] == boardArray[8]))
			|| ((boardArray[2] == boardArray[4]) && (boardArray[2] == boardArray[6]))) {
		printf("Player %d has won! Do you want to play again? r/R to restart. anything else quits\n",
				player);
		scanf("%c", &input);

		if (input == 'R' || input == 'r') {
			playGame(boardArray);
		} else {
			exit(0);
		}
	}
}

/* Check to see which player is placing their token, then ask where and place it. */
void playerTurn(char *boardArray, int player) {
	char *choicePtr;
	char choice;
	int intChoice;
	int done = 0;

	if (player == 1) {
		printf("Player 1: Enter which space you would like to play your token. q/Q to quit\n");
		printBoard(boardArray);
		choicePtr = &choice;

		// Part of checking to make sure they enter a valid spot to play in.
		while (done != 1) {
			scanf("%c", &choice);
			intChoice = atoi(choicePtr);
			// Checking to make sure spot isnt already played
			if (boardArray[intChoice - 1] != 'C' && boardArray[intChoice - 1] != 'U') {
				switch (choice) {
					case '1':
						boardArray[0] = 'C';
						done = 1;
						break;
					case '2':
						boardArray[1] = 'C';
						done = 1;
						break;
					case '3':
						boardArray[2] = 'C';
						done = 1;
						break;
					case '4':
						boardArray[3] = 'C';
						done = 1;
						break;
					case '5':
						boardArray[4] = 'C';
						done = 1;
						break;
					case '6':
						boardArray[5] = 'C';
						done = 1;
						break;
					case '7':
						boardArray[6] = 'C';
						done = 1;
						break;
					case '8':
						boardArray[7] = 'C';
						done = 1;
						break;
					case '9':
						boardArray[8] = 'C';
						done = 1;
						break;
					case 'q':
					case 'Q':
						exit(0);
					default:
						printf(" Enter a valid choice, 1-9, q/Q to quit: ");
						break;
				}
			} else {
				printf("That spot is already played, choose another.\n");
			}
			winCheck(boardArray, player);
		}
	}

	if (player == 2) {
		printf("Player 2: Enter which space you would like to play your token. q/Q to quit\n");
		printBoard(boardArray);
		choicePtr = &choice;
		// Part of checking to make sure they enter a valid spot to play in.
		while (done != 1) {
			scanf("%c", &choice);
			intChoice = atoi(choicePtr);

			// Checking to make sure spot isnt already played
			if (boardArray[intChoice - 1] != 'C' && boardArray[intChoice - 1] != 'U') {
				switch (choice) {
					case '1':
						boardArray[0] = 'U';
						done = 1;
						break;
					case '2':
						boardArray[1] = 'U';
						done = 1;
						break;
					case '3':
						boardArray[2] = 'U';
						done = 1;
						break;
					case '4':
						boardArray[3] = 'U';
						done = 1;
						break;
					case '5':
						boardArray[4] = 'U';
						done = 1;
						break;
					case '6':
						boardArray[5] = 'U';
						done = 1;
						break;
					case '7':
						boardArray[6] = 'U';
						done = 1;
						break;
					case '8':
						boardArray[7] = 'U';
						done = 1;
						break;
					case '9':
						boardArray[8] = 'U';
						done = 1;
						break;
					case 'q':
					case 'Q':
						exit(0);
					default:
						printf
							(" Enter a valid choice, 1-9, q/Q to quit: ");
						break;
				}
			} else {
				printf("That spot is already played, choose another.\n");
			}

			winCheck(boardArray, player);
		}
	}
}

/* TODO: find a good way to have this loop for the expected amoun of turns,
 * if we run out of turns we can declare a tie, check win condition after
 * each token placement. */
int main(int argc, char **argv) {
	char boardArray[9];
	playGame(boardArray);
	return EXIT_SUCCESS;
}
